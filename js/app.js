//importa funções de outras arquivos JS
Vue.use(ShopifyProducts);

const store = new Vuex.Store({
    state: {
        products : {}
    },
    mutations: {
        products(state, payload){
            state.products = payload;
        }
    }
});

const router = new VueRouter({
    routes : [
        {
            path: '/products/:slug',
            component: ProductPage
        },
        {
            path: '/404',
            alias: '*',
            component: PageNotFound
        }
    ]
});

const app = new Vue({
    el: '#app',
    store,
    router,

    created() {    
        d3.csv('https://raw.githubusercontent.com/shopifypartners/shopify-product-csvs-and-images/master/csv-files/bicycles.csv', 
            (error, data) => {     
                let products = this.$formatProducts(data);
                console.log(products);
                this.$store.commit('products', products);
            });  
        }
});